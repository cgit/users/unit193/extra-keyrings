#!/bin/bash

# requires: GPG 2.1

# dumps out our minimal keys, useful to create nicely formatted
# debian/upstream/signin-keys.asc in our packages


set -eu -o pipefail

removetemp () { rm -rf "$gpghome" && echo "$gpghome removed" >&2 ; }
gpghome="$(mktemp -d)"
trap removetemp EXIT

GPG=("gpg" "--homedir" "$gpghome" "--batch")

keys=($@)
for key in "${keys[@]}";do
   if [ -f "$key" ];then
      action="--import"
      keyIDs+=($(gpg --with-colons "$key" | cut -d: -f5))
   else
      action="--recv-keys"
      keyIDs+=("$key")
   fi
   "${GPG[@]}" \
       --keyserver keyserver.ubuntu.com \
       $action \
       "$key"
done

# print some information about the keys
# (--fingerprint twice so the fingeprint of the subkeys is printed too)
"${GPG[@]}" \
    --list-keys \
    --keyid-format none \
    --with-subkey-fingerprint \
    --list-options no-show-keyring \
    | tail -n +3

# re-export them
"${GPG[@]}" \
    --export \
    --armor \
    --export-options export-minimal \
    "${keyIDs[@]}"
